package com.example.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        nav_host_fragment.findNavController().navigatorProvider.addNavigator(
            CustomFragmentNavigator(
                this,
                nav_host_fragment.childFragmentManager,
                R.id.nav_host_fragment
            )
        )
        nav_host_fragment.findNavController().setGraph(R.navigation.nav_graph)
    }
}